# Introduction

This is my personal knowledge gardon.

## Why Build a Wiki?

At this pivotal moment in my PhD journey, I find myself engaging in crucial self-reflection. While I've derived great satisfaction from collaborative projects and helping others, I must acknowledge that my research capabilities haven't evolved as substantially as I had envisioned. This realization has prompted me to carefully weigh my options: pursuing a stable career path versus committing fully to my aspiration of becoming a researcher. After thorough introspection, I've concluded that the intellectual rigor and endless curiosity inherent in research align perfectly with my professional aspirations.

To bridge the gap between my current capabilities and my goals, I've decided to pause my studies for a comprehensive review of my academic foundation. This wiki serves two primary purposes: first, to systematically review and document my knowledge from undergraduate and postgraduate studies, creating a clear inventory of my capabilities; second, to map out the boundaries of my expertise and establish more concrete objectives for future growth. This process of documentation and reflection will help crystallize my understanding of both my strengths and areas needing development.

Additionally, this wiki will serve as a resource for my brother and friends pursuing computer science education. By organizing and sharing my learning materials, I hope to accelerate their learning journey and contribute to their success.

This is a long-term project that may take several years to fully develop. Rather than viewing this as a limitation, I see it as an opportunity for continuous refinement and expansion of knowledge. The wiki will grow and evolve alongside my understanding, serving as a living document of my academic and professional development.

In today's rapidly evolving landscape, maintaining intellectual curiosity and embracing lifelong learning is crucial. Through this commitment to personal growth and knowledge sharing, I aim to not only enhance my own capabilities but also contribute meaningfully to the broader research community.

## Arrangement

To enhance accessibility and navigation, I've structured this knowledge base according to subject domains. The core focus is on computer science, where my expertise is most developed, covering a comprehensive spectrum from fundamental theories to advanced development practices.

The subsequent sections delve into natural sciences, particularly physics and mathematics - fields that continually captivate my interest and form the foundation of many advanced concepts in computer science.

The final portion of this wiki encompasses practical skills and valuable insights gathered through experience. This systematic organization aims to provide readers with a clear, logical pathway through various topics, facilitating both targeted learning and broader exploration.

## Tools in this projects

### mdbook

mdbook is a rust project that works like a static website generator, but more than a generator. It also provides a service for hosting and building static websites. For more information, see [mdbook](https://rust-lang.github.io/mdBook).

### slmd

All of the topics in the table of content are flexible and I often add new topics here. To make them more organizable, I use a sort tool called [slmd](https://github.com/lqez/slmd), which helps me sorts all the unordered lists of topics.

### mdbook-toc

[mdbook-toc](https://github.com/badboy/mdbook-toc) helps me generate 'Table of Content' on the heading of some long pages. This might also be helpful to get a quick overview of the knowledge in a certain long wiki page.

### mdbook-admonish

[mdbook-admonish](https://github.com/tommilligan/mdbook-admonish) creates blocks of text in an interesting and good-looking style, such as:

```admonish example
This is an example
```

```admonish note
This is a note
```

```admonish warning
This is a warning
```

