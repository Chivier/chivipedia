# Writing

[路过图床 - 免费图片上传, 专业图片外链, 免费公共图床 (imgse.com)](https://imgse.com/) - Upload Images

[SM.MS - Simple Free Image Hosting](https://sm.ms/login) - Upload Images

[ASCIIFlow](https://asciiflow.com/#/) - Plot simple images

[Admonitions - Material for MkDocs (squidfunk.github.io)](https://squidfunk.github.io/mkdocs-material/reference/admonitions/) - Mkdocs Publisher

[实践自部署Overleaf | Sparktour's](https://sparktour.me/2021/04/02/self-host-overleaf/) - Self-hosted overleaf

[Free Text to Speech Online with Realistic AI Voices (naturalreaders.com)](https://www.naturalreaders.com/online/) - Text to speech

[Beautiful Free Images & Pictures | Unsplash](https://unsplash.com/) - Free images for your article

[WantWords 反向词典](https://wantwords.net/) - Find alternative vocabulary

[论文可视化配色简易指南 - 少数派 (sspai.com)](https://sspai.com/post/75989) - Find beautiful colours

[33台词 - 通过台词找影片素材 (agilestudio.cn)](https://33.agilestudio.cn/) - Find lines in films or scripts

[Write & Improve with Cambridge (writeandimprove.com)](https://writeandimprove.com/) - Improve your writing

