# Reading

## Books

- [Z-Library Project (singlelogin.me)](https://singlelogin.me/)
- [Library Genesis (libgen.is)](https://libgen.is/)
- [Packt](https://www.packtpub.com/)
  - Programming Books, eBooks & Videos for Developers
- [Free Magazines PDF](https://freemagazinespdf.com/)
  -  English PDF Magazine Download
- [Allitebooks](https://allitebook.xyz/)
  - Download Free IT E-books
- [Jiumo Search 鸠摩搜索](https://www.jiumodiary.com/)

## Audio Books

- [Welcome to Lit2Go ETC (usf.edu)](https://etc.usf.edu/lit2go/)
- [LibriVox | free public domain audiobooks](https://librivox.org/)

## Ebook Management

### Offline

- [calibre - E-book management (calibre-ebook.com)](https://calibre-ebook.com/)

### Online

- [使用Calibre-web构建线上图书馆 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/119327134)
- [janeczku/calibre-web (github)](https://github.com/janeczku/calibre-web)
  - Web app for browsing, reading and downloading eBooks stored in a Calibre
