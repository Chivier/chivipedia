# Researh Tools

- [All You Need Is Arxiv Search](https://www.arxiv.dev/)
- [DeepL Translate API | Machine Translation Technology](https://www.deepl.com/pro-api?cta=header-pro-api)
- [Stream-dataflow acceleration | Connected Papers](https://www.connectedpapers.com/main/8a5bd97d3b4c8d22311f1aa4436e19948738001a/Stream%20dataflow-acceleration/graph)
- [科研者之家-scihub-SCI写作助手-国家自然科学基金-期刊查询](https://www.home-for-researchers.com/static/index.html#/)
- [The latest in Machine Learning | Papers With Code](https://paperswithcode.com/)
- [dblp: Browse Journals](https://dblp.uni-trier.de/db/journals/index.html)
