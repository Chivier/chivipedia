# Language

## English

### Vocabulary

[扇贝，知道你在改变 (shanbay.com)](https://web.shanbay.com/web/main)

[扇贝单词小助手 (greasyfork.org)](https://greasyfork.org/zh-CN/scripts/419996-扇贝单词小助手)

These two tools work together perfectly!

[Kaiyiwing/qwerty-learner: 为键盘工作者设计的单词记忆与英语肌肉记忆锻炼软件 / Words learning and English muscle memory training software designed for keyboard workers (github.com)](https://github.com/Kaiyiwing/qwerty-learner)

This tool is also fantastic for vocabulary memorising.

### Reading

[Free magazine | Download PDF magazines and ebook free USA, UK, Australia and other (magazinebis.co)](https://magazinebis.co/)

