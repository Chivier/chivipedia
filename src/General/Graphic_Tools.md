# Graphic Tools

## Plotting Tools

- [Mermaid](https://mermaid.js.org/#/)
  - Diagramming and charting tool
  - Flow graph, Gantt graph, pie chart and so on...
  - Programmable
- [Miro](https://miro.com/signup/)
  - Online Whiteboard for Visual Collaboration
  - Mindmap + online sharing + collaberate editting
- [HoloViews](https://holoviews.org/)
  - Python plotting library
- [Veusz](https://veusz.github.io/)
  - Python API, a scientific plotting package
- [Earth Lab](https://www.earthdatascience.org/tutorials/visualize-digital-elevation-model-contours-matplotlib/)
- [Mapbox](https://docs.mapbox.com/mapbox-gl-js/example/)
- [ECharts](https://echarts.apache.org/examples/zh/index.html)
- [quiver: a modern commutative diagram editor](https://q.uiver.app/)
  - Support tikz


## Plotting Examples

- [25 matplotlib examples](https://mp.weixin.qq.com/s?__biz=Mzg4NTUzNzE5OQ==&mid=2247520585&idx=1&sn=df6d07884d03a7c75669b06117484b34&chksm=cfa5b189f8d2389f21cc6186a882e21555405aeb5af16384284574f69b94abe6ddbf335fc2cc&scene=126&&sessionid=1658040392#rd)
- [GNUplot](https://gnuplot.sourceforge.net/demo_5.5/)
  - GNU plotting demos

## Image Generation

- [Text to Handwriting](https://saurabhdaware.github.io/text-to-handwriting/)
- [ASCIIFlow](https://asciiflow.com/#/)

## Image Resources

- [Awesome Wallpapers](https://wallhaven.cc/)
- [Unsplash](https://unsplash.com/)
- [Icon search](https://icongo.github.io/#/search)
