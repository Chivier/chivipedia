# Architecture

## Must read

[Computer Architecture: A Quantitative Approach](https://acs.pub.ro/~cpop/SMPA/Computer%20Architecture,%20Sixth%20Edition_%20A%20Quantitative%20Approach%20(%20PDFDrive%20).pdf) - Must read

[A New Golden Age for Computer Architecture](https://www.doc.ic.ac.uk/~wl/teachlocal/arch/papers/cacm19golden-age.pdf)

## Course and Resources

[Onur Mutlu's Course](https://www.youtube.com/onurmutlulectures?themeRefresh=1) - Best architecture course.

[AMD architecture report](https://web.archive.org/web/20140328140823/http://amd-dev.wpengine.netdna-cdn.com/wordpress/media/2012/10/hsa10.pdf)

[Silicon Talk - 老石谈芯](https://www.youtube.com/@laoshi_tec) - One of the most insightful Youtuber about Chips
