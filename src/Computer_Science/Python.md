# Python

## Tutorials

[3小时快速入门 3小时快速入门Python](https://www.bilibili.com/video/BV1944y1x7SW/?spm_id_from=333.337.search-card.all.click)  - Tutorial, a quick start (Chinese)

[Python for Beginners - Learn Python in 1 Hour - YouTube](https://www.youtube.com/watch?v=kqtD5dpn9C8) - Another quick start (English)

[Python Crash Course by ehmatthes](https://ehmatthes.github.io/pcc/) - My favourite Python book for beginners

[Fluent Python Book (oreilly.com)](https://www.oreilly.com/library/view/fluent-python/9781491946237/) - Improvement book for python



## Tools

Python is very suitable for script editing, and some tools can improve your programming experience.



[Project Jupyter | Home](https://jupyter.org/) - Jupyter, a must have tool for python

[psf/black: The uncompromising Python code formatter (github.com)](https://github.com/psf/black) - My favourite formatter

[Jupyter and the future of IPython — IPython](https://ipython.org/) - Works better for CLI work

[Matplotlib — Visualization with Python](https://matplotlib.org/) - A wonderful visualisation tool

[Numba: A High Performance Python Compiler (pydata.org)](http://numba.pydata.org/) - Make python run faster



## Virtualenv

Virtual environment(sometimes call venv or virtulenv), is essential for python developers, they always want to keep their packages clean and tidy. And a separated environment can prevent packages from conflicting from each other.



### `python-venv`

[How to Set Up a Virtual Environment in Python – And Why It's Useful (freecodecamp.org)](https://www.freecodecamp.org/news/how-to-setup-virtual-environments-in-python/)

[venv Python 3.11.3 documentation](https://docs.python.org/3/library/venv.html)



### `pyenv`(recommended)

[pyenv/pyenv: Simple Python version management (github.com)](https://github.com/pyenv/pyenv)

[Rebuild Virtualenv with pyenv | by Chivier Humber | Medium](https://medium.com/@chivier.humber_15513/rebuild-virtualenv-with-pyenv-2f85aede30e)



### `anaconda/miniconda`

[Why Use Anaconda? : r/Python (reddit.com)](https://www.reddit.com/r/Python/comments/betkoj/why_use_anaconda/)

[Installation — Anaconda documentation](https://docs.anaconda.com/free/anaconda/install/index.html)

[Miniconda — conda documentation](https://docs.conda.io/en/latest/miniconda.html)





