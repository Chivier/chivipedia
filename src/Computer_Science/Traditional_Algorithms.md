# Traditional Algorithms

This part list out some resources for *classical or traditional* algorithm and data structures.



[Hello 算法 (hello-algo.com)](https://www.hello-algo.com/)

[The Algorithms (the-algorithms.com)](https://the-algorithms.com/) - the best wikipedia for algorithms

[Learn Data Structures and Algorithms | DSA Tutorial - GeeksforGeeks](https://www.geeksforgeeks.org/learn-data-structures-and-algorithms-dsa-tutorial/?ref=shm) - a quick and simple algorithm book

[OI Wiki (oi-wiki.org)](https://en.oi-wiki.org/) - ACM/OI competition wiki



Just learning and reading algorithms is not a good study strategy. I strongly recommend readers to try and write codes on Leetcode or other platforms.





