# Programming Language

Before listing out all the programming lang's resources, some common things and some of my learning principles are ought to put out here.



## Why learn a Programming Language?

A programming language has its own drama in the world of computer science. Some of them are used for agile development, with extremely simple grammar and rules. While some of them are chasing peak performance on some specific programming platforms, such as C++ and Cuda. So on the page dedicated to each programming language, some typical template projects are also listed as examples.

Last but not least, I would like to emphasise that the best way to learn a programming language is to practise on your own computer. As you gain confidence and practical experience, you will enjoy a boost in the fast lane.



## General sites

- [W3Schools Online Web Tutorials](https://www.w3schools.com/)

- [菜鸟教程](https://www.runoob.com/)
- [Online Courses on Udemy](https://www.udemy.com/)

- https://leetcode.com/ Practise More!

