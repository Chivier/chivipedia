# Traditional Compiler

## Tools

[Compiler Explorer (godbolt.org)](https://gcc.godbolt.org/) - Most important tools for me



## GNU

[GCC, the GNU Compiler Collection - GNU Project](https://gcc.gnu.org/)

[GCC and Make - A Tutorial on how to compile, link and build C/C++ applications (ntu.edu.sg)](https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html)

[ListOfCompilerBooks - GCC Wiki (gnu.org)](https://gcc.gnu.org/wiki/ListOfCompilerBooks) - GCC Book List

[GCC Resource Center for GCC Internals (iitb.ac.in)](http://www.cse.iitb.ac.in/grc/index.php?page=videos)

[Compilers: Principles, Techniques, and Tools (2nd Edition) by Aho, Alfred V., Lam, Monica S., Sethi, Ravi, Ullman, Jeffrey (2006) Hardcover: Amazon.co.uk: Books](https://www.amazon.co.uk/Compilers-Principles-Techniques-Jeffrey-Hardcover/dp/B00M0RMUGS/ref=sr_1_4?s=books&ie=UTF8&qid=1522745601&sr=1-4&keywords=Compilers - Principles%2C Techniques and Tools&dpID=51Gi1IFAI9L&preST=_SY291_BO1%2C204%2C203%2C200_QL40_&dpSrc=srch)  - (optional) this book is a little bit out-of-date, but classical theory on compilers are still worth learning



## LLVM

[The LLVM Compiler Infrastructure Project](https://llvm.org/)

[About — LLVM 17.0.0git documentation](https://llvm.org/docs/) - LLVM grows quickly these years, remember always look for the correct version of documentation from github mirror

[xiaoweiChen/Learn-LLVM-12: 《Learn LLVM 12》的非专业个人翻译 (github.com)](https://github.com/xiaoweiChen/Learn-LLVM-12) - (Chinese translation) original version is on Packet

[xiaoweiChen/LLVM-Techniques-Tips-and-Best-Practies: 《LLVM Techniques, Tips, and Best Practices》的非专业个人翻译 (github.com)](https://github.com/xiaoweiChen/LLVM-Techniques-Tips-and-Best-Practies) - (Chinese translation) original version is on Packet

[Programming Language with LLVM [1/20] Introduction to LLVM IR and tools - YouTube](https://www.youtube.com/watch?v=Lvc8qx8ukOI)
