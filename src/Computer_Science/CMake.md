# CMake

[CMake](https://cmake.org/) - Official website

[An Introduction to Modern CMake · Modern CMake (cliutils.gitlab.io)](https://cliutils.gitlab.io/modern-cmake/)

[CMake Tutorial. I have read numerous documentations and… | by Onur Dündar | Medium](https://medium.com/@onur.dundar1/cmake-tutorial-585dd180109b) - A quick start

[CMake 课程介绍_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1vL41117xz/?spm_id_from=333.788&vd_source=cc14657430a87459650a2c1384d56dd9) - A very nice online cmake course

[xiaoweiChen/Modern-CMake-for-Cpp: 《Modern CMake for C++》的非专业个人翻译 (github.com)](https://github.com/xiaoweiChen/Modern-CMake-for-Cpp)

[xiaoweiChen/CMake-Best-Practices: 《CMake Best Practices》的非专业个人翻译 (github.com)](https://github.com/xiaoweiChen/CMake-Best-Practices)
