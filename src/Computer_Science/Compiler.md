# Compiler

## Compiler General Knowledge

General knowledge

[Compiler - Wikipedia](https://en.wikipedia.org/wiki/Compiler) - Wikipedia gives a general knowledge of compilers.

[Engineering a Compiler - Google Books](https://www.google.com.hk/books/edition/_/WxTozgEACAAJ?hl=en&sa=X&ved=2ahUKEwipzcef7cz-AhVbklYBHVgTD_kQ7_IDegQIJxAD) - The best compiler book I have ever read about compilers.

[Compilers | Course | Stanford Online](https://online.stanford.edu/courses/soe-ycscs1-compilers)

## LLVM

If you wanna know something deeper or something in industry about compiler, you must learn LLVM.

[LLVM in 100s](https://www.youtube.com/watch?v=BT2Cv-Tjq7Q) - What is LLVM?

[LLVM quick start](https://llvm.org/docs/GettingStarted.html) - Quick start to know what is LLVM

[Learn LLVM 12](https://github.com/PacktPublishing/Learn-LLVM-12) - It will never outdated. This book demostrate the overview and the structure of LLVM.

## Compiler Tools

[Compiler Explorer (godbolt.org)](https://gcc.godbolt.org/) - Best online compiler explorer.

